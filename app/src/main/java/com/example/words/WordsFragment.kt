package com.example.words

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.words.databinding.FragmentWordsBinding


class WordsFragment : Fragment() {
    private lateinit var binding: FragmentWordsBinding

    var layoutManager: RecyclerView.LayoutManager? = null
    var recyclerView: RecyclerView? = null
    private lateinit var adapter: WordsAdapter
    private var layoutGrid: MenuItem? = null
    private var layoutRow: MenuItem? = null
    private var isEditing = true

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.item_layout, menu)
        layoutGrid = menu.findItem(R.id.layout_grid)
        layoutRow = menu.findItem(R.id.layout_row)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.layout_grid -> {
                layoutGrid?.isVisible = false
                layoutRow?.isVisible = true
                isEditing = false
                initGridDisplay()
                return true
            }
            R.id.layout_row -> {
                layoutGrid?.isVisible = true
                layoutRow?.isVisible = false
                isEditing = true
                initListDisplay()
                return true
            }
        }
        return false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentWordsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity?)?.setActionBarTitle("Words")

        adapter = WordsAdapter(Word.word){position ->  onItemClick(position) }
        recyclerView = binding.rvWords
        this.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recyclerView!!.layoutManager = layoutManager
        recyclerView!!.adapter = adapter
    }


    private fun initListDisplay() {
        val layoutManager = LinearLayoutManager(context)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        recyclerView!!.layoutManager = layoutManager
    }

    private fun initGridDisplay() {
        val layoutManager = GridLayoutManager(context, 2)
        layoutManager.orientation = GridLayoutManager.VERTICAL
        recyclerView!!.layoutManager = layoutManager
    }

    private fun onItemClick(position: String) {
        val args = Bundle()
        val frag = WordsWithStart()
        when(position){
            "A" -> {
                args.putString("word", position)
            }
            "B" -> {
                args.putString("word", position)
            }
            "C" -> {
                args.putString("word", position)
            }
            "D" -> {
                args.putString("word", position)
            }
            "E" -> {
                args.putString("word", position)
            }
            "F" -> {
                args.putString("word", position)
            }
            "G" -> {
                args.putString("word", position)
            }
            "H" -> {
                args.putString("word", position)
            }
            "I" -> {
                args.putString("word", position)
            }
            "J" -> {
                args.putString("word", position)
            }
            "K" -> {
                args.putString("word", position)
            }
            "L" -> {
                args.putString("word", position)
            }
            "M" -> {
                args.putString("word", position)
            }
            "N" -> {
                args.putString("word", position)
            }
            "O" -> {
                args.putString("word", position)
            }
            "P" -> {
                args.putString("word", position)
            }
            "Q" -> {
                args.putString("word", position)
            }
            "R" -> {
                args.putString("word", position)
            }
            "S" -> {
                args.putString("word", position)
            }
            "T" -> {
                args.putString("word", position)
            }
            "U" -> {
                args.putString("word", position)
            }
            "V" -> {
                args.putString("word", position)
            }
            "W" -> {
                args.putString("word", position)
            }
            "X" -> {
                args.putString("word", position)
            }
            "Y" -> {
                args.putString("word", position)
            }
            "Z" -> {
                args.putString("word", position)
            }

        }
        frag.arguments = args
        val ft: FragmentTransaction = requireFragmentManager().beginTransaction()
        ft.addToBackStack(null)
        ft.replace(R.id.words_fragment, frag)
        ft.setReorderingAllowed(true)
        ft.commit()
    }

}