package com.example.words


class Word {

    companion object{
        var word = arrayListOf(
            MyWords("A"),
            MyWords("B"),
            MyWords("C"),
            MyWords("D"),
            MyWords("E"),
            MyWords("F"),
            MyWords("G"),
            MyWords("H"),
            MyWords("I"),
            MyWords("J"),
            MyWords("K"),
            MyWords("L"),
            MyWords("M"),
            MyWords("N"),
            MyWords("O"),
            MyWords("P"),
            MyWords("Q"),
            MyWords("R"),
            MyWords("S"),
            MyWords("T"),
            MyWords("U"),
            MyWords("V"),
            MyWords("W"),
            MyWords("X"),
            MyWords("Y"),
            MyWords("Z"),
        )

        var A = arrayListOf<MyWords>(
            MyWords("Abjad"),
            MyWords("Aktivitas"),
            MyWords( "Amfibi"),
        )
        var B = arrayListOf<MyWords>(
            MyWords("Belum"),
            MyWords("Besok"),
            MyWords( "Blanko"),
        )
        var C = arrayListOf<MyWords>(
            MyWords("Cabai"),
            MyWords("Cendekiawan"),
            MyWords( "Cacing"),
        )
        var D = arrayListOf<MyWords>(
            MyWords("Dadu"),
            MyWords("Daftar"),
            MyWords( "Dekorasi"),
        )
        var E = arrayListOf<MyWords>(
            MyWords("Elang"),
            MyWords("Emas"),
            MyWords( "Efektif"),
        )
        var F = arrayListOf<MyWords>(
            MyWords("Februari"),
            MyWords("Formal"),
            MyWords( "Foto"),
        )
        var G = arrayListOf<MyWords>(
            MyWords("Gajah"),
            MyWords("Gubuk"),
            MyWords( "Gudang"),
        )
        var H = arrayListOf<MyWords>(
            MyWords("Hari"),
            MyWords("Hati"),
            MyWords( "Hantu"),
        )
        var I = arrayListOf<MyWords>(
            MyWords("Iguana"),
            MyWords("Indera"),
            MyWords( "Imaginasi"),
        )
        var J = arrayListOf<MyWords>(
            MyWords("Jejak"),
            MyWords("Jangkar"),
            MyWords( "Jadwal"),
        )
        var K = arrayListOf<MyWords>(
            MyWords("Kaki"),
            MyWords("Kaktus"),
            MyWords( "Karena"),
        )
        var L = arrayListOf<MyWords>(
            MyWords("Lelah"),
            MyWords("Lalu"),
            MyWords( "Lapar"),
        )
        var M = arrayListOf<MyWords>(
            MyWords("Matahari"),
            MyWords("Mencoba"),
            MyWords( "Merawat"),
        )
        var N = arrayListOf<MyWords>(
            MyWords("Nasihat"),
            MyWords("Negeri"),
            MyWords( "Nomor"),
        )
        var O = arrayListOf<MyWords>(
            MyWords("Opera"),
            MyWords("Objek"),
            MyWords( "Orang"),
        )
        var P = arrayListOf<MyWords>(
            MyWords("Palu"),
            MyWords("Paku"),
            MyWords( "Pulang"),
        )
        var Q = arrayListOf<MyWords>(
            MyWords("Quota"),
            MyWords("Qatar"),
            MyWords( "Quartz"),
        )
        var R = arrayListOf<MyWords>(
            MyWords("Roti"),
            MyWords("Rabu"),
            MyWords( "Roda"),
        )
        var S = arrayListOf<MyWords>(
            MyWords("Sepatu"),
            MyWords("Selalu"),
            MyWords( "Selesai"),
        )
        var T = arrayListOf<MyWords>(
            MyWords("Tanda"),
            MyWords("Tulang"),
            MyWords( "Tato"),
        )
        var U = arrayListOf<MyWords>(
            MyWords("Uang"),
            MyWords("Untuk"),
            MyWords( "Untung"),
        )
        var V = arrayListOf<MyWords>(
            MyWords("Vespa"),
            MyWords("Vega"),
            MyWords( "Voli"),
        )
        var W = arrayListOf<MyWords>(
            MyWords("Wol"),
            MyWords("Walau"),
            MyWords( "Wafat"),
        )
        var X = arrayListOf<MyWords>(
            MyWords("X"),
            MyWords("X"),
            MyWords( "X"),
        )
        var Y = arrayListOf<MyWords>(
            MyWords("Ya"),
            MyWords("Yobel"),
            MyWords( "Yoyo"),
        )
        var Z = arrayListOf<MyWords>(
            MyWords("Zebra"),
            MyWords("Zoo"),
            MyWords( "Zaman"),
        )

    }
}
