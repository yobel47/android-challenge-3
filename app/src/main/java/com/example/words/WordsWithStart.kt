package com.example.words

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.words.databinding.FragmentWordsWithStartBinding


class WordsWithStart : Fragment() {

    private lateinit var binding: FragmentWordsWithStartBinding
    var layoutManager: RecyclerView.LayoutManager? = null
    var recyclerView: RecyclerView? = null
    private lateinit var adapter: WordsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentWordsWithStartBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val bundle = this.arguments
        (activity as MainActivity?)?.setActionBarTitle("Words That Starts With "+bundle!!.getString("word").toString())
        recyclerView = binding.rvWordsWithStart
        this.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recyclerView!!.layoutManager = layoutManager
        when(bundle!!.getString("word")){
            "A" -> {
                adapter = WordsAdapter(Word.A){position ->  onItemClick(position)}
            }
            "B" -> {
                adapter = WordsAdapter(Word.B){position ->  onItemClick(position) }
            }
            "C" -> {
                adapter = WordsAdapter(Word.C){position ->  onItemClick(position) }
            }
            "D" -> {
                adapter = WordsAdapter(Word.D){position ->  onItemClick(position) }
            }
            "E" -> {
                adapter = WordsAdapter(Word.E){position ->  onItemClick(position) }
            }
            "F" -> {
                adapter = WordsAdapter(Word.F){position ->  onItemClick(position) }
            }
            "G" -> {
                adapter = WordsAdapter(Word.G){position ->  onItemClick(position) }
            }
            "H" -> {
                adapter = WordsAdapter(Word.H){position ->  onItemClick(position) }
            }
            "I" -> {
                adapter = WordsAdapter(Word.I){position ->  onItemClick(position) }
            }
            "J" -> {
                adapter = WordsAdapter(Word.J){position ->  onItemClick(position) }
            }
            "K" -> {
                adapter = WordsAdapter(Word.K){position ->  onItemClick(position) }
            }
            "L" -> {
                adapter = WordsAdapter(Word.L){position ->  onItemClick(position) }
            }
            "M" -> {
                adapter = WordsAdapter(Word.M){position ->  onItemClick(position) }
            }
            "N" -> {
                adapter = WordsAdapter(Word.N){position ->  onItemClick(position) }
            }
            "O" -> {
                adapter = WordsAdapter(Word.O){position ->  onItemClick(position) }
            }
            "P" -> {
                adapter = WordsAdapter(Word.P){position ->  onItemClick(position) }
            }
            "Q" -> {
                adapter = WordsAdapter(Word.Q){position ->  onItemClick(position) }
            }
            "R" -> {
                adapter = WordsAdapter(Word.R){position ->  onItemClick(position) }
            }
            "S" -> {
                adapter = WordsAdapter(Word.S){position ->  onItemClick(position) }
            }
            "T" -> {
                adapter = WordsAdapter(Word.T){position ->  onItemClick(position) }
            }
            "U" -> {
                adapter = WordsAdapter(Word.U){position ->  onItemClick(position) }
            }
            "V" -> {
                adapter = WordsAdapter(Word.V){position ->  onItemClick(position) }
            }
            "W" -> {
                adapter = WordsAdapter(Word.W){position ->  onItemClick(position) }
            }
            "X" -> {
                adapter = WordsAdapter(Word.X){position ->  onItemClick(position) }
            }
            "Y" -> {
                adapter = WordsAdapter(Word.Y){position ->  onItemClick(position) }
            }
            "Z" -> {
                adapter = WordsAdapter(Word.Z){position ->  onItemClick(position) }
            }
            else -> {
                adapter = WordsAdapter(Word.Z){position ->  onItemClick(position) }
            }
        }
        recyclerView!!.adapter = adapter
    }

    private fun onItemClick(position: String) {
        val intent = Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://www.google.com/search?q=$position"))
        startActivity(intent)
    }

}