package com.example.words

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.words.databinding.MenuItemBinding

class WordsAdapter(private val listWords: List<MyWords>, private val onItemClick: (position: String) -> Unit) : RecyclerView.Adapter<WordsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemStoryBinding = MenuItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(itemStoryBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listWords[position], onItemClick)
    }

    override fun getItemCount() = listWords.size

    class ViewHolder(private val binding: MenuItemBinding) : RecyclerView.ViewHolder(binding.root) {
        private lateinit var words: MyWords

        fun bind(words: MyWords, onItemClick: (position: String) -> Unit) {
            this.words = words
            binding.btnWords.text = words.word
            binding.btnWords.setOnClickListener {
                onItemClick(words.word)
            }
        }

    }
}