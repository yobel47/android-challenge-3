package com.example.words

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import com.example.words.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {

    private lateinit var _binding : ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(_binding.root)

        val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
        ft.add(R.id.words_fragment, WordsFragment())
        ft.setReorderingAllowed(true)
        ft.commit()
    }

    fun setActionBarTitle(title: String?) {
        supportActionBar!!.title = title
    }

}